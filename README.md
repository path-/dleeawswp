# WordPress automation in AWS

# Assumptions

1. CloudFormation is the initial kickoff point
2. There are no automation pipelines handling the overall stack deployments
3. The purpose is for a one-off use for individual, smaller clients

# How to use:

1. Using the CloudFormation json template located in this repository, kick off a CloudFormation stack. The private git scenario template will prompt for a repository password whereas the public git scenario template will not. Default values have been added for easy use.
2. Name the Stack then fill out the necessary CloudFormation parameters.
    * autoupdisable: A boolean to disable for auto update in WordPress
    * coreupdatelevel: A boolean to allow for auto core update in WordPress
    * databasename: A string to name the MariaDB database for WordPress
    * dbpass: A string for the user password to access the database
    * dbport: Numbers to dictate which port the database will be hosted on
    * dbusername: A string for the username to access the database
    * GitDirectory: Defining which directory the git repository will be cloned on
    * GitPassword ( for private git template ): Password to access the git repository
    * GitRepository: The git repository URL
    * KeyName: A dropdown list of the ssh keypairs created in AWS for the VM instance
    * SSHLocation: IP CIDR range whitelisting SSH access
    * webport: Which port WordPress will be hosted on
    * wpadmin: The WordPress admin account name
    * wpemail: The WordPress admin account email address
    * wppass: The WordPress admin account password
    * wptitle: The WordPress title
3. Once the CloudFormation parameters have been validated, no further configuration is necessary for this scenario.
4. The external IP of the VM will be posted in the 'outputs' section when viewing the stack details.

# What it's doing
### CloudFormation
1. Creates the necessary infrastructure elements to support a VM to handle internet connections. This current template is hardcoded to use an Amazon Linux instance.
2. Gathers the parameters to build out the MariaDB database, the git repository, and WordPress.
3. Yum updates, installs git and Ansible, git clones the repository.
4. Creates an Ansible 'group\_vars' file based off of the parameters
5. Kicks off Ansible playbook

### Ansible
1. Installs the LAMP (mariadb, nginx, php-fpm) stack
2. Uses templates to make any necessary changes to the configuration files for the LAMP stack and places them in the corresponding directories.
2. Downloads the latest WordPress installation media.
3. Captures WordPress salt.
4. Unarchives WordPress at designated directory (hardcoded at /srv/)
5. Creates a database for WordPress and configures with parameters inputted during CloudFormation stack deployment.
6. Runs first-time setup for WordPress in CLI
7. Installs Codilight-lite template for WordPress (personal choice) then activates it

# Extras (In a real production scenario)

1. Subnets would be separated for dev, testing, and production.
2. CloudFormation stack deployments would be handled by an automation pipeline.
3. Presumably there would be WordPress templates already configured speicifcally for a client to automatically install.
4. Separate database server (depending on the expected traffic and use case of the instance).
5. Load balanced (depending on the expected traffic and use case of the instance).
6. Use domain name instead of public IP.
7. Multiple tiers of tested, replicable architecture for different traffic and price range.
8. Not have WordPress user password stored as plain text. Possibly pass it through as a secure string as a prompt when running Ansible.
